%% This code is provided to accompany the YouTube video here: https://youtu.be/tKa-3PYgjDw
%% If you use this code for a publication or to make content, please give attribution to Joel Rosenfeld and link to the YouTube video

figure

plot(cos(0:0.01:2*pi),sin(0:0.01:2*pi),'LineWidth',3);
[X,Y] = getpts;
InputPoints = X' + sqrt(-1)*Y';


plot(cos(0:0.01:2*pi),sin(0:0.01:2*pi),'LineWidth',3);
[X,Y] = getpts;
OutputPoints = X' + sqrt(-1)*Y';

Q = (1 - OutputPoints'*OutputPoints)./(1-InputPoints'*InputPoints);

if(min(eigs(Q)) < 0)
    display('Not gonna work, buddy!');
else
    display("Let's Do this!");
    
    InTrans = zeros(length(InputPoints));
    OutTrans = zeros(length(OutputPoints));
    
    InTrans(1,:) = InputPoints;
    OutTrans(1,:) = OutputPoints;
    
    for i = 2:length(InputPoints)
        for j = 1:length(InputPoints) - (i-1)
            InTrans(i,j) = ...
                (InTrans(i-1,j) - InTrans(i-1,length(InputPoints) - (i-2)))/...
                (1-InTrans(i-1,length(InputPoints) - (i-2))'*InTrans(i-1,j));
            OutTrans(i,j) = ...
                (OutTrans(i-1,j) - OutTrans(i-1,length(InputPoints) - (i-2)))/...
                (1-OutTrans(i-1,length(InputPoints) - (i-2))'*OutTrans(i-1,j));
            OutTrans(i,j) = OutTrans(i,j)/InTrans(i,j);
        end
    end
    
    z0 = InTrans(end,1);
    w0 = OutTrans(end,1);
    
    Blaschke1 = @(z) (z-z0)./(1-z0'*z);
    Blaschke2 = @(z) (z+w0)./(1+w0'*z);
    
    g = @(z) Blaschke2(Blaschke1(z));
    
    for i = length(InputPoints) - 1:-1:1
        g = @(z) z.*g(z);
        
        zn = InTrans(i,length(InputPoints)-(i-1));
        
        wn = OutTrans(i,length(InputPoints)-(i-1));
        
        g = @(z) (g((z - zn)./(1-zn'*z)) + wn)./(1+wn'.*g((z - zn)./(1-zn'*z)));
    end

    x = -1:0.01:1;
    [Xplot,Yplot] = meshgrid(x);
    
    Zplot = zeros(size(Xplot));
    
    for i = 1:length(x)
        for j = 1:length(x)
            Zplot(i,j) = g(Xplot(i,j) + sqrt(-1)*Yplot(i,j));
        end
    end
    
    surf(Xplot,Yplot,min(1,abs(Zplot)),'edgecolor','none');
    hold on;
    plot3(real(InputPoints),imag(InputPoints),abs(OutputPoints),'ro','LineWidth',3);
    hold off;
end